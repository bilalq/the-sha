#Develop.ios Responsibility Assigning SHA#

For all decisions in which bias may factor, we shall smite it with the SHA.

The way is such:

* Take the bitcoin high value of the day as listed on mtgox.com
* Add that value to the month(1-12), day(0-31), and year(e.g., 2012)
* Run this value through a SHA256 function
* Loop through these characters until we find one mapped to a person
* In the unlikely event that no character in the hash maps to one of these, we will hash the hash

Run instructions:

    `./sha`

Seriously.


##The people map:##
* 0 => Shamoon,
* 1 => Cory,
* 2 => Bilal,
* 3 => Alex (girl),
* 4 => Arthur,
* 5 => Alexandra,
* 6 => Ciara,
* 7 => Ronald,
* 8 => Duraien,
* 9 => Nick,
* a => Jon,
* b => Raj,
* c => Mike
